# Fit free throw data as binary logistic regression with variables of score difference, time left in game, minutes played this game, and home or away

import MySQLdb
import numpy as np
import pickle
import gzip
import os
import statsmodels.discrete.discrete_model as sm

def fitfteff():
    pcrnames = np.loadtxt("playercoachrefnames", dtype= 'string') # List of players, coaches, and referees
    for k in range(len(pcrnames)):
        fteff = {} # Dictionary to contain mapping of player to training data
        ftefffits = {} # Dictionary to contain mapping of player to fitted parameters
        fteffavgs = {} # Dictionary to contain mapping of player to FT percentages
        ftefflen = {} # Dictionary to contain mapping of player to number of FTs attempted
        ft = MySQLdb.connect("remote IP address", "root", "password", "fteff") # Connect to remote MySQL db fteff for previous seasons' data on FT efficiency
        ftc = ft.cursor()
        sql = "SELECT * FROM " + pcrnames[k] # Query to select all data from player's table of past FTs
        try:
            print("Trying to enter " + pcrnames[k] + ' into fteff')
            ftc.execute(sql)
            results = ftc.fetchall()
            fteff[pcrnames[k]] = np.zeros((len(results), 12))
            for m in range(len(results)):
                fteff[pcrnames[k]][m][0] = results[m][0] # Binary response variable (0 if missed FT, 1 if made FT)
                fteff[pcrnames[k]][m][1] = 1 # Bias term
                fteff[pcrnames[k]][m][2] = results[m][1]**2 # Difference in score ^ 2
                fteff[pcrnames[k]][m][3] = results[m][1] # Difference in score
                fteff[pcrnames[k]][m][4] = results[m][2]**2 # Time left in game ^ 2
                fteff[pcrnames[k]][m][5] = results[m][2] # Time left in game
                fteff[pcrnames[k]][m][6] = results[m][3] # Minutes played this game
                fteff[pcrnames[k]][m][7] = results[m][4] # Home or away
                fteff[pcrnames[k]][m][8] = results[m][1]*results[m][2] # Interaction term between difference in score and time left in game
                fteff[pcrnames[k]][m][9] = results[m][1]*results[m][2]**2 # Interaction term between difference in score and time left in game
                fteff[pcrnames[k]][m][10] = (results[m][1]**2)*results[m][2] # Interaction term between difference in score and time left in game
                fteff[pcrnames[k]][m][11] = (results[m][1]**2)*results[m][2]**2 # Interaction term between difference in score and time left in game
            model = sm.Logit(np.ravel(fteff[pcrnames[k]][:,[0]]), fteff[ pcrnames[k]][:,range(1, 12)]) # Fit data as logistic regression
            ftefffits[pcrnames[k]] = model.fit(method = 'powell', maxiter = 1000, maxfun = 2000) # Use powell minimization algorithm with maximum function calls set at 2000
            fteffavgs[pcrnames[k]] = list(fteff[pcrnames[k]][:,[0]]).count(1)/float(len(fteff[pcrnames[k]][:,[0]])) # Store FT percentage
            ftefflen[pcrnames[k]] = len(fteff[pcrnames[k]][:,[0]]) # Store number of FT attempts
        except: # Ignore coaches and referees
            print('Could not enter ' + pcrnames[k] + ' into fteff')
            continue
        ftc.close()
        ft.close()
        # Save dictionaries as pickled objects
        output = gzip.open(os.path.expanduser('~/Path/To/Folder/' + pcrnames[k] + 'ftefffits.pkl'), 'wb')
        pickle.dump(ftefffits, output)
        output = gzip.open(os.path.expanduser('~/Path/To/Folder/' + pcrnames[k] + 'fteffavgs.pkl'), 'wb')
        pickle.dump(fteffavgs, output)
        output = gzip.open(os.path.expanduser('~/Path/To/Folder/' + pcrnames[k] + 'ftefflen.pkl'), 'wb')
        pickle.dump(ftefflen, output)
        output.close()
