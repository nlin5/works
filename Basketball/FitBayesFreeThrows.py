# Perform Bayesian logistic regression using prior parameters from past seasons and this season's data

import bayes_logistic
import numpy as np
import statsmodels.discrete.discrete_model as sm
from collections import Counter

def bayesfteff(results, wprior):
    fteff = np.zeros((len(results), 12))
    for m in range(len(results)):
        fteff[m][0] = results[m][0] # Binary response variable (0 if missed FT, 1 if made FT)
        fteff[m][1] = 1 # Bias term
        fteff[m][2] = results[m][1]**2 # Difference in score ^ 2
        fteff[m][3] = results[m][1] # Difference in score
        fteff[m][4] = results[m][2]**2 # Time left in game ^ 2
        fteff[m][5] = results[m][2] # Time left in game
        fteff[m][6] = results[m][3] # Minutes played this game
        fteff[m][7] = results[m][4] # Home or away
        fteff[m][8] = results[m][1]*results[m][2] # Interaction term between difference in score and time left in game
        fteff[m][9] = results[m][1]*results[m][2]**2 # Interaction term between difference in score and time left in game
        fteff[m][10] = (results[m][1]**2)*results[m][2] # Interaction term between difference in score and time left in game
        fteff[m][11] = (results[m][1]**2)*results[m][2]**2 # Interaction term between difference in score and time left in game
    if wprior is None or hprior is None: # If player is a rookie and has no prior data, fit on current data
        llen = len(fteff[:,[0]])
        try:
            avgs = list(fteff[:,[0]]).count(1)/float(len(fteff[:,[0]]))
        except ZeroDivisionError:
            avgs = 0
        try:
            model = sm.Logit(np.ravel(fteff[:,[0]]), fteff[:,range(1, 12)]) # Fit data into logistic regression
            newfits = model.fit(method = 'powell', maxiter = 1000, maxfun = 2000)
            wpost = list(newfits.params)
            hpost = newfits.model.hessian(newfits.params)
            return [wpost, hpost, avgs, llen] # Return parameters, Hessian, FT%, and FT attempts
        except:
            return [None, None, avgs, llen]
    llen = len(fteff[:,[0]])
    try:
        avgs = list(fteff[:,[0]]).count(1)/float(len(fteff[:,[0]]))
    except ZeroDivisionError:
        avgs = 0
    try:
        hprior = np.diag(np.ones(11))*0.001 # Initialize diagonal Hessian with low regularization
        w_posterior, H_posterior = bayes_logistic.fit_bayes_logistic(np.ravel(fteff[:,[0]]), fteff[:,range(1, 12)], np.array(wprior), hprior) # Perform Bayesian logistic regression
        return [w_posterior, H_posterior, avgs, llen] # Return posterior parameters, posterior Hessian, FT% and FT attempts
    except:
        return [wprior, hprior, avgs, llen]
