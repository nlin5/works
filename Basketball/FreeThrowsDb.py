import requests
import MySQLdb

def createfteff():
    db = MySQLdb.connect("localhost","root","password","fteff")
    cursor = db.cursor()
    url = 'http://stats.nba.com/stats/commonallplayers?IsOnlyCurrentSeason=1&LeagueID=00&Season=2016-17'
    u_a = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36"
    response = requests.get(url, headers={"USER-AGENT":u_a})
    players = response.json()["resultSets"]
    playernames = players[0]["rowSet"]
    for k in range(len(playernames)):
        name = playernames[k][2]
        name = name.replace(" ","")
        name = name.replace("-","")
        name = name.replace(".","")
        name = name.replace("'","")
        cursor.execute("DROP TABLE IF EXISTS " + name)
        sql = "CREATE TABLE " + name + "(IF_MADE_FT INT, SCORE_DIFF INT, TIME_LEFT_IN_GAME DOUBLE, MIN_PLAYED_THIS_GAME DOUBLE, HOME_OR_AWAY INT )"
        cursor.execute(sql)
