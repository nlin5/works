# This script accesses stats.nba.com's API to parse play-by-play JSON to obtain data on free throw efficiency
# To be run daily as a cron job throughout the 2016-17 season to obtain data on last night's games

import requests
import MySQLdb
from datetime import datetime, date, timedelta

def main():
    u_a = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36" # User agent for Python's requests to parse JSON
    today = date.today() # datetime object form of today's date
    yesterday = today - timedelta(days=1)
    thegames = [] # list to contain game IDs of last night's games
    for k in range(1,1231): # 1230 games in an NBA season
        kk = 21600000 + k # 216 header refers to the regular season of the 2016-17 NBA season
        dateurl = 'http://stats.nba.com/stats/boxscoresummaryv2?GameID=00' + str(kk) # stats.NBA.com request url format for obtaining date of a game
        try:
            dateresponse = requests.get(dateurl, headers={"USER-AGENT":u_a})
            dates = dateresponse.json()["resultSets"]
        except:
            break
        thedate = dates[4]["rowSet"][0][0]
        thedate = thedate.replace(",", "")
        thedate = datetime.strptime(thedate, '%A %B %d %Y').date()
        if thedate == yesterday:
            thegames.append(k) # Append game ID to list of last night's games to parse

    fteffdb = MySQLdb.connect("localhost", "root", "password", "fteff") # Connect to localhost database named fteff
    cursorfteff = fteffdb.cursor()                
    for z in range(min(thegames),max(thegames)+1): 
        print(z)
        aingame = [] # List of players currently in game for team a, the visiting team
        bingame = [] # List of players currently in game for team b, the home team
        aplay = [] # List of players on team a who will play in the game
        astint = [0] * 5 # List of stint lengths for current players in the game for team a
        bplay = [] # List of players on team b who will play in the game
        bstint = [0] * 5 # List of stint lengths for current players in the game for team b
        quarters = [] # List of indices to mark the end of a quarter. Used to determine if a game goes to OT
        scorediff = 0 # Difference in score between team a and team b. Positive scorediff indicates team b is winning
        zz = 21600000 + z

        starturl = 'http://stats.nba.com/stats/boxscoretraditionalv2?EndPeriod=10&EndRange=0&GameID=00' + str(zz) + '&RangeType=2&Season=2016-17&SeasonType=Regular+Season&StartPeriod=1&StartRange=0' # Find players who started the 1st quarter
        startresponse = requests.get(starturl, headers={"USER-AGENT":u_a})
        players = startresponse.json()["resultSets"]
        playernames = players[0]["rowSet"]
        # teama is visitor, teamb is home
        teama = playernames[0][2] # need to see if either teama or teamb are min to be changed to minn for mySQL
        teamb = playernames[9][2]          
        for k in range(0,5):
            aingame.append(playernames[k][5])
        for k in range(5,10):
            bingame.append(playernames[k][5])

        secqstarturl = 'http://stats.nba.com/stats/boxscoretraditionalv2?EndPeriod=10&EndRange=7474&GameID=00' + str(zz) + '&RangeType=2&Season=2016-17&SeasonType=Regular+Season&StartPeriod=1&StartRange=7206' # Find players who started the 2nd quarter
        secqstartresponse = requests.get(secqstarturl, headers={"USER-AGENT":u_a})
        secqplayers = secqstartresponse.json()["resultSets"]
        secqplayernames = secqplayers[0]["rowSet"]
        if len(secqplayernames) != 10:
            print(len(secqplayernames))
            print(teama,teamb)
            continue

        thrqstarturl = 'http://stats.nba.com/stats/boxscoretraditionalv2?EndPeriod=10&EndRange=14886&GameID=00' + str(zz) + '&RangeType=2&Season=2016-17&SeasonType=Regular+Season&StartPeriod=1&StartRange=14449' # Find players who started the 3rd quarter
        thrqstartresponse = requests.get(thrqstarturl, headers={"USER-AGENT":u_a})
        thrqplayers = thrqstartresponse.json()["resultSets"]
        thrqplayernames = thrqplayers[0]["rowSet"]
        if len(thrqplayernames) != 10:
            print(len(thrqplayernames))
            print(teama,teamb)
            continue

        fouqstarturl = 'http://stats.nba.com/stats/boxscoretraditionalv2?EndPeriod=10&EndRange=21880&GameID=00' + str(zz) + '&RangeType=2&Season=2016-17&SeasonType=Regular+Season&StartPeriod=1&StartRange=21618' # Find players who started the 4th quarter
        fouqstartresponse = requests.get(fouqstarturl, headers={"USER-AGENT":u_a})
        fouqplayers = fouqstartresponse.json()["resultSets"]
        fouqplayernames = fouqplayers[0]["rowSet"]
        if len(fouqplayernames) != 10:
            print(len(fouqplayernames))
            print(teama,teamb)
            continue

        wholeboxurl = 'http://stats.nba.com/stats/boxscoretraditionalv2?EndPeriod=10&EndRange=28800&GameID=00' + str(zz) + '&RangeType=0&Season=2016-17&SeasonType=Regular+Season&StartPeriod=1&StartRange=0' 
        wholeboxresponse = requests.get(wholeboxurl, headers={"USER-AGENT":u_a})
        wholeboxplayers = wholeboxresponse.json()["resultSets"]
        wholeboxplayernames = wholeboxplayers[0]["rowSet"]
        for k in range(len(wholeboxplayernames)):
            if wholeboxplayernames[k][2] == teama and wholeboxplayernames[k][8] is not None:
                aplay.append(wholeboxplayernames[k][5]) # find all players on team a who played in the game
            elif wholeboxplayernames[k][2] == teamb and wholeboxplayernames[k][8] is not None:
                bplay.append(wholeboxplayernames[k][5]) # find all players on team b who played in the game
        aplaytotmin = [0] * len(aplay) # total minutes played by each player in team a
        bplaytotmin = [0] * len(bplay) # total minutes played by each player in team b

        pbpurl = 'http://stats.nba.com/stats/playbyplayv2?EndPeriod=10&EndRange=55800&GameID=00' + str(zz) + '&RangeType=2&Season=2016-17&SeasonType=Regular+Season&StartPeriod=1&StartRange=0' # Log of all plays in the game
        pbpresponse = requests.get(pbpurl, headers={"USER-AGENT":u_a})
        pbpparse = pbpresponse.json()["resultSets"]
        pbpplays = pbpparse[0]["rowSet"]
        for k in range(6,len(pbpplays)):
            if pbpplays[k][2] == 13: # marks the end of a quarter
                quarters.append(k)
        if len(quarters) > 4: # Do not parse this game if it went to OT
            print('OT')
            continue
        c = 1
        for m in range(4):
            for k in range(c,quarters[m]+1):
                timeleftinq = pbpplays[k][6].split(":")
                timeleftinq = int(timeleftinq[0]) + int(timeleftinq[1])/60.0
                nexttime = pbpplays[k-1][6].split(":")
                nexttime = int(nexttime[0]) + int(nexttime[1])/60.0
                if nexttime - timeleftinq < 0:
                    continue
                timeleftingame = timeleftinq + (3-m)*12
                for n in range(5):
                    astint[n] = astint[n] + nexttime - timeleftinq
                    bstint[n] = bstint[n] + nexttime - timeleftinq
                for n in range(len(aplay)):
                    for o in range(5):
                        if aplay[n] == aingame[o]:
                            aplaytotmin[n] = aplaytotmin[n] + nexttime - timeleftinq
                for n in range(len(bplay)):
                    for o in range(5):
                        if bplay[n] == bingame[o]:
                            bplaytotmin[n] = bplaytotmin[n] + nexttime - timeleftinq
                if pbpplays[k][2] == 8: # coach makes a substitution
                    if pbpplays[k][18] == teama:
                        old = pbpplays[k][14]
                        new = pbpplays[k][21]
                        for x in range(5):
                            if aingame[x] == old:
                                aingame[x] = new
                                astint[x] = 0
                    if pbpplays[k][18] == teamb:
                        old = pbpplays[k][14]
                        new = pbpplays[k][21]
                        for x in range(5):
                            if bingame[x] == old:
                                bingame[x] = new
                                bstint[x] = 0
                if pbpplays[k][2] == 3: # player shoots FT
                    playermade = pbpplays[k][14] # the player who shot the FT
                    if pbpplays[k][18] == teama: # the player who shot FT is on teama
                        homeaway = 0 # player is visitor
                        description = pbpplays[k][9] # description of what the play was
                        if description.find("MISS ") > -1:
                            madeormiss = 0
                        else:
                            madeormiss = 1
                        for u in range(len(aplaytotmin)):
                            if aplay[u] == playermade:
                                minplayedthisgame = aplaytotmin[u]
                        scorediff = -scorediff # Because player who shot FT is on team a, reverse scorediff
                        playermade = playermade.replace(" ","")
                        playermade = playermade.replace("-","")
                        playermade = playermade.replace(".","")
                        playermade = playermade.replace("'","")
                        fteffsql = "INSERT INTO " + playermade + "(IF_MADE_FT, SCORE_DIFF, TIME_LEFT_IN_GAME, MIN_PLAYED_THIS_GAME, HOME_OR_AWAY) VALUES ('%d', '%d', '%f', '%f', '%d')" % (madeormiss, scorediff, timeleftingame, minplayedthisgame, homeaway)
                        try:
                            print('Trying to enter ' + playermade + ' into fteff')
                            cursorfteff.execute(fteffsql)
                            fteffdb.commit()
                        except:
                            print('Could not enter ' + playermade)
                            fteffdb.rollback()
                        scorediff = -scorediff
                    else:
                        homeaway = 1
                        description = pbpplays[k][7]
                        if description.find("MISS ") > -1:
                            madeormiss = 0
                        else:
                            madeormiss = 1
                        for u in range(len(bplaytotmin)):
                            if bplay[u] == playermade:
                                minplayedthisgame = bplaytotmin[u]
                        playermade = playermade.replace(" ","")
                        playermade = playermade.replace("-","")
                        playermade = playermade.replace(".","")
                        playermade = playermade.replace("'","")
                        fteffsql = "INSERT INTO " + playermade + "(IF_MADE_FT, SCORE_DIFF, TIME_LEFT_IN_GAME, MIN_PLAYED_THIS_GAME, HOME_OR_AWAY) VALUES ('%d', '%d', '%f', '%f', '%d')" % (madeormiss, scorediff, timeleftingame, minplayedthisgame, homeaway)
                        try:
                            print('Trying to enter ' + playermade + ' into fteff')
                            cursorfteff.execute(fteffsql)
                            fteffdb.commit()
                        except:
                            print('Could not enter ' + playermade)
                            fteffdb.rollback()
                scoremargin = pbpplays[k][11]
                if scoremargin is not None:
                    if scoremargin == 'TIE':
                        scorediff = 0
                    else:
                        scorediff = int(scoremargin)
            c = quarters[m]+2
            if m == 0: # Replace players in game with new players at end of each quarter, if any substitutions were made
                aingame = []
                bingame = []
                astint = [0] * 5
                bstint = [0] * 5
                for k in range(0,5):
                    aingame.append(secqplayernames[k][5])
                for k in range(5,10):
                    bingame.append(secqplayernames[k][5])
            elif m == 1:
                aingame = []
                bingame = []
                astint = [0] * 5
                bstint = [0] * 5
                for k in range(0,5):
                    aingame.append(thrqplayernames[k][5])
                for k in range(5,10):
                    bingame.append(thrqplayernames[k][5])
            elif m == 2:
                aingame = []
                bingame = []
                astint = [0] * 5
                bstint = [0] * 5
                for k in range(0,5):
                    aingame.append(fouqplayernames[k][5])
                for k in range(5,10):
                    bingame.append(fouqplayernames[k][5])

main()
