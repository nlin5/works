% Demonstrates naive linear learning from error in series of perturbations in
% stable, neutral, and transient environments

pert = importdata('perturbations.txt'); % import perturbation data
yhat = 0; % start naive learner
stableyhats = zeros(1,100);
stableerr = zeros(1,100);
neutralyhats = zeros(1,100);
neutralerr = zeros(1,100);
transientyhats = zeros(1,100);
transienterr = zeros(1,100);
for k = 1:100 % finding stable yhats
    stableyhats(k) = yhat;
    yhat = yhat + 0.1*(pert(1,k)-yhat); % update yhat
    stableerr(k) = 0.1*(pert(1,k)-yhat); % accumulate error
end
yhat = 0;
for k = 1:100 % finding neutral yhats
    neutralyhats(k) = yhat;
    yhat = yhat + 0.1*(pert(2,k)-yhat);
    neutralerr(k) = 0.1*(pert(2,k)-yhat);
end
yhat = 0;
for k = 1:100 % finding transient yhats
    transientyhats(k) = yhat;
    yhat = yhat + 0.1*(pert(3,k)-yhat);
    transienterr(k) = 0.1*(pert(3,k)-yhat);
end
plot(1:100,stableyhats)
hold on
plot(1:100,pert(1,:))
title('Yhats for Stable Perturbation Trials')
xlabel('Trial')
ylabel('Yhat')
hold off
figure()
plot(1:100,neutralyhats)
hold on
plot(1:100,pert(2,:))
title('Yhats for Neutral Perturbation Trials')
xlabel('Trial')
ylabel('Yhat')
hold off
figure()
plot(1:100,transientyhats)
hold on
plot(1:100,pert(3,:))
title('Yhats for Transient Perturbation Trials')
xlabel('Trial')
ylabel('Yhat')
hold off
figure()
plot(1:100,stableerr)
hold on
plot(1:100,neutralerr)
plot(1:100,transienterr)
title('Learning from Error for each Trial')
xlabel('Trial')
ylabel('Learning from Error')
hold off
