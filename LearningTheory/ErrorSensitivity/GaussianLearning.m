% Demonstrates naive Gaussian learning with basis set in series of
% perturbations in stable, neutral, and transient environment

pert = importdata('perturbations.txt'); % import perturbation data
yhat = 0;
stableyhats = zeros(1,100);
stableerr = zeros(1,100);
neutralyhats = zeros(1,100);
neutralerr = zeros(1,100);
transientyhats = zeros(1,100);
transienterr = zeros(1,100);
g = [-1 -0.5 0 0.5 1]; % create Gaussian basis for error space
w = repmat(0.0403,1,5); % calculated w for first error sensitivity to be 0.1
for k = 1:100 % finding stable yhats
    if k == 1
        stableyhats(k) = yhat;
        error = pert(1,k)-yhat;
        gout = exp(-((error-g).^2)/(2*0.5^2)); % calculate error based on Gaussian basis
        sens = gout*w'; % calculate error sensitivity 
        stableerr(k) = sens*error;
        yhat = yhat + sens*error; % update yhat
    else
        stableyhats(k) = yhat;
        w = w + 0.001*sign(error*(pert(1,k)-yhat))*(gout/(gout*gout')); % update weight vector
        error = pert(1,k)-yhat;
        gout = exp(-((error-g).^2)/(2*0.5^2));
        sens = gout*w';
        stableerr(k) = sens*error;
        yhat = yhat + sens*error;
    end
end
yhat = 0;
g = [-1 -0.5 0 0.5 1];
w = repmat(0.0403,1,5);
for k = 1:100 % finding neutral yhats
    if k == 1
        neutralyhats(k) = yhat;
        error = pert(2,k)-yhat;
        gout = exp(-((error-g).^2)/(2*0.5^2));
        sens = gout*w';
        neutralerr(k) = sens*error;
        yhat = yhat + sens*error;
    else
        neutralyhats(k) = yhat;
        w = w + 0.001*sign(error*(pert(2,k)-yhat))*(gout/(gout*gout'));
        error = pert(2,k)-yhat; 
        gout = exp(-((error-g).^2)/(2*0.5^2));
        sens = gout*w';
        neutralerr(k) = sens*error;
        yhat = yhat + sens*error;
    end
end
yhat = 0;
g = [-1 -0.5 0 0.5 1];
w = repmat(0.0403,1,5);
for k = 1:100 % finding transient yhats
    if k == 1
        transientyhats(k) = yhat;
        error = pert(3,k)-yhat;
        gout = exp(-((error-g).^2)/(2*0.5^2));
        sens = gout*w';
        transienterr(k) = sens*error;
        yhat = yhat + sens*error;
    else
        transientyhats(k) = yhat;
        w = w + 0.001*sign(error*(pert(3,k)-yhat))*(gout/(gout*gout'));
        error = pert(3,k)-yhat; 
        gout = exp(-((error-g).^2)/(2*0.5^2));
        sens = gout*w';
        transienterr(k) = sens*error;
        yhat = yhat + sens*error;
    end
end
plot(1:100,stableyhats)
hold on
plot(1:100,pert(1,:))
title('Yhats for Stable Perturbation Trials')
xlabel('Trial')
ylabel('Yhat')
hold off
figure()
plot(1:100,neutralyhats)
hold on
plot(1:100,pert(2,:))
title('Yhats for Neutral Perturbation Trials')
xlabel('Trial')
ylabel('Yhat')
hold off
figure()
plot(1:100,transientyhats)
hold on
plot(1:100,pert(3,:))
title('Yhats for Transient Perturbation Trials')
xlabel('Trial')
ylabel('Yhat')
hold off
figure()
plot(1:100,stableerr)
hold on
plot(1:100,neutralerr)
plot(1:100,transienterr)
title('Learning from Error for each Trial')
xlabel('Trial')
ylabel('Learning from Error')
hold off
