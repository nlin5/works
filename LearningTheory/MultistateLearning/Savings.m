% Demo of Kojima's 2004 experiment of savings using Kalman Filter to model
% paradigm of slow, medium, and fast time scales with many hidden states 
% w(n) = Aw(n-1) + ew(n); ew ~ N(0, Q)
% y(n) = x(n)'w(n) + ey(n); ey ~ N(0, sigma^2)

priorw = zeros(1,3);
priorp = [0.5 0 0; 0 0.75 0; 0 0 1.5];
A = [0.9995 0 0; 0 0.98 0; 0 0 0.75];
Q = [0.00001 0 0; 0 0.0005 0; 0 0 0.002];
x = [1 1 1];
y = zeros(1,600);
yhat = zeros(1,600);
w = zeros(3,600);
p = zeros(3,600);
for k = 1:300
    gain = (priorp*x')/(x*priorp*x' + 0.2);
    y(k) = normrnd(0,0.2);
    yhat(k) = x*priorw';
    postw = priorw + transpose(gain*(y(k)-yhat(k)));
    w(:,k) = postw;
    postp = ([1 0 0; 0 1 0; 0 0 1] - gain*x)*priorp;
    p(:,k) = vertcat(postp(1,1),postp(2,2),postp(3,3));
    priorw = transpose(A*postw');
    priorp = A*postp*A' + Q;
end 
for k = 301:500
    gain = (priorp*x')/(x*priorp*x' + 0.2);
    y(k) = normrnd(1,0.2);
    yhat(k) = x*priorw';
    postw = priorw + transpose(gain*(y(k)-yhat(k)));
    w(:,k) = postw;
    postp = ([1 0 0; 0 1 0; 0 0 1] - gain*x)*priorp;
    p(:,k) = vertcat(postp(1,1),postp(2,2),postp(3,3));
    priorw = transpose(A*postw');
    priorp = A*postp*A' + Q;
end
k = 501;
while x*priorw' > 0
    gain = (priorp*x')/(x*priorp*x' + 0.2);
    y(k) = normrnd(-1,0.2);
    yhat(k) = x*priorw';
    postw = priorw + transpose(gain*(y(k)-yhat(k)));
    w(:,k) = postw;
    postp = ([1 0 0; 0 1 0; 0 0 1] - gain*x)*priorp;
    p(:,k) = vertcat(postp(1,1),postp(2,2),postp(3,3));
    priorw = transpose(A*postw');
    priorp = A*postp*A' + Q;
    k = k + 1;
end
for z = k:600
    gain = (priorp*x')/(x*priorp*x' + 0.2);
    y(z) = normrnd(1,0.2);
    yhat(z) = x*priorw';
    postw = priorw + transpose(gain*(y(z)-yhat(z)));
    w(:,z) = postw;
    postp = ([1 0 0; 0 1 0; 0 0 1] - gain*x)*priorp;
    p(:,z) = vertcat(postp(1,1),postp(2,2),postp(3,3));
    priorw = transpose(A*postw');
    priorp = A*postp*A' + Q;
end
figure()
plot(1:600,y)
hold on
plot(1:600,yhat)
title('Y for each trial')
xlabel('Trial')
ylabel('Y')
hold off
figure()
plot(1:600,w(1,:))
hold on
plot(1:600,w(2,:))
plot(1:600,w(3,:))
title('Ws for each trial')
xlabel('Trial')
ylabel('Ws')
hold off
figure()
plot(1:600,p(1,:))
hold on
plot(1:600,p(2,:))
plot(1:600,p(3,:))
title('Ps for each trial')
xlabel('Trial')
ylabel('Ps')
hold off