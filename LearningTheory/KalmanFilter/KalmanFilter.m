% Equations given with signal-dependent noise
% x(k+1) = Ax(k) + ex(k)
% y(k) = H(x(k) + es(k)) + ey(k)
% The variables change at different states, which last for 200 iterations

load Kalman_HW8_dat
H = [1; 1];
priorx = x0; % set prior state
priorp = [1 0 ; 0 1]; % set prior variance
yhats = zeros(1,800); % empty vector to be filled with predictions
gainks = zeros(2,800); % empty vector to be filled with Kalman gains
postxs = {}; % empty cell to be filled with posterior x at steps 1, 200, 400, 600, 800
postps = {}; % empty cell to be filled with posterior p at steps 1, 200, 400, 600, 800
for k = 1:800
    if k <= 200
        gainks(:,k) = (priorp*H)/(transpose(H)*priorp*H + transpose(H)*[1 0 ; 0 1]*H + 1); % compute gain of system
        yhats(k) = transpose(H)*priorx; % make prediction
        postx = priorx + gainks(:,k)*(y(k) - yhats(k)); % filtering to generate posterior x
        postp = ([1 0; 0 1] - gainks(:,k)*transpose(H))*priorp; % filtering to generate posterior p
        if k == 1 || k == 200 % if step 1 or 200, add to cell array
            postxs{end+1} = postx;
            postps{end+1} = postp;
        end
        priorp = [0.5 0; 0 0.3]*postp*transpose([0.5 0; 0 0.3]) + [1 0 ; 0 1]; % update for next iteration
        priorx = [0.5 0; 0 0.3]*postx; % update for next iteration
    elseif k > 200 && k <= 400
        gainks(:,k) = (priorp*H)/(transpose(H)*priorp*H + transpose(H)*[1 0 ; 0 1]*H + 1);
        yhats(k) = transpose(H)*priorx;
        postx = priorx + gainks(:,k)*(y(k) - yhats(k));
        postp = ([1 0; 0 1] - gainks(:,k)*transpose(H))*priorp;
        if k == 400
            postxs{end+1} = postx;
            postps{end+1} = postp;
        end
        priorp = [1 0; 0 1]*postp*transpose([1 0; 0 1]) + [25 0 ; 0 25];
        priorx = [1 0; 0 1]*postx;
    elseif k > 400 && k <= 600
        gainks(:,k) = (priorp*H)/(transpose(H)*priorp*H + transpose(H)*[1 0 ; 0 1]*H + 25);
        yhats(k) = transpose(H)*priorx;
        postx = priorx + gainks(:,k)*(y(k) - yhats(k));
        postp = ([1 0; 0 1] - gainks(:,k)*transpose(H))*priorp;
        if k == 600
            postxs{end+1} = postx;
            postps{end+1} = postp;
        end
        priorp = [1 0; 0 1]*postp*transpose([1 0; 0 1]) + [1 0 ; 0 1];
        priorx = [1 0; 0 1]*postx;
    else
        gainks(:,k) = (priorp*H)/(transpose(H)*priorp*H + transpose(H)*[25 0 ; 0 25]*H + 1);
        yhats(k) = transpose(H)*priorx;
        postx = priorx + gainks(:,k)*(y(k) - yhats(k));
        postp = ([1 0; 0 1] - gainks(:,k)*transpose(H))*priorp;
        if k == 800
            postxs{end+1} = postx;
            postps{end+1} = postp;
        end
        priorp = [1 0; 0 1]*postp*transpose([1 0; 0 1]) + [1 0 ; 0 1];
        priorx = [1 0; 0 1]*postx;
    end
end
figure() % plot predicted and actual measurements
plot(1:800,y)
hold on
plot(1:800,yhats)
title('Comparison of Predicted and Actual Measurements')
xlabel('Iteration')
ylabel('Y')
hold off