% The code uses regression to classify two categories of data into 1 or 0

import = importdata('classify_regress.txt'); % import data set
x = horzcat(ones(200,1),import.data(:,1:2)); % find training set and add column of 1's so wT*x = w0 + w1*x1 + w2*x2
y = import.data(:,3); % classes of training set
w = inv(transpose(x)*x)*transpose(x)*y; % find weight vector using normal equation
figure(); % open figure to plot classes
scatter(import.data(1:100,1),import.data(1:100,2),[],[1 0 0]) % create scatter plot of 0 classes first and color them red
hold on;
scatter(import.data(101:200,1),import.data(101:200,2),[],[0 1 0]) % add scatter of 1 classes and color them green
refline(-1.04, 2.4); % calculated equation of line that separates class in attached summary
title('Regression Classification');
xlabel('x1');
ylabel('x2');
hold off;
