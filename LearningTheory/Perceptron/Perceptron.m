% The code uses the perceptron algorithm to try to predict whether a viewer will like a certain movie or not, using a training set of 200 movies and iterating through it 1000 times. It records the number of wrong predictions and plots the errors.

input = importdata('input.dat'); % import training data of features for 200 movies a viewer watched
output = importdata('output.dat'); % import truths of whether the viewer liked them. +1 meant liked, -1 meant disliked
w = zeros(1,10); % starting with intial weight vector of 0 to be updated based on accuracy of predictions
errors = zeros(1,1000); % 1X1000 vector to keep track of number of incorrect predictions for each of 1000 loops
for k = 1:1000 % repeat prediction process through all 200 movies 1000 times
    e = 0; % set initial error to 0 to be updated with each iteration
    for m = 1:200 % 200 movies to be iterated through
        yhat = sign(w*transpose(input(m,:))); % prediction of whether person liked movie based on sign of weight multiplied by features, x
        if yhat ~= output(m) % if incorrect prediction
            w = w + 2*output(m)*input(m,:); % update weight vector by adding 2*(y-yhat)*x, where y-yhat = 2y because y = -yhat since prediction is wrong
            e = e + 1; % update total error count because of wrong prediction
        end
    end
    errors(k) = e; % add total error to 1X1000 vector for each of 1000 repeats
end
figure()
plot(1:1000,errors) % plot number of wrong predictions as a function of iteration number 
