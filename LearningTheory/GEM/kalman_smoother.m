function [xnN,PnN,Pnp1nN] = kalman_smoother(parameters,y,r,threshold)

% This function computes the smoothed Kalman estimates for the
% expected state, variance of the state, and covariance of consecutive
% states

%%% Inputs: y - the observed subject reaching direction
%%%         r - the sequence of perturbations
%%%         threshold - the trial number at which the a and b can
%%%                     change value
%%%         parameters - the current parameter set
%%% Outputs: xnN - N x 1 array containing the smoothed Kalman
%%%                expected states
%%%          PnN - N x 1 array containing the smoothed Kalman
%%%                state variances
%%%          Pnp1nN - N x 1 array containing the smoother Kalman
%%%                   covariance of x(n) and x(n+1) given all the data up
%%%                   to N. Its last entry is NaN, because we do not care
%%%                   abour Pnp1nN(N+1,N).

% stores input variables using descriptive names
% uses these parameter values in your code below
a1 = parameters(1);
a2 = parameters(2);
b1 = parameters(3);
b2 = parameters(4);
c = parameters(5);
sigmax2 = parameters(6);
sigmau2 = parameters(7);
x1 = parameters(8);
V1 = parameters(9);
priorx = x1;
priorv = V1;
% determines the number of trials
N = length(y);

% allocates space for each output (each is described above)
xnN = zeros(N,1);
PnN = zeros(N,1);
Pnp1nN = zeros(N,1);
Pnp1nN(end) = NaN;
priorxs = zeros(N,1);
postxs = zeros(N,1);
priorvs = zeros(N,1);
postvs = zeros(N,1);
% Implementing a forward Kalman filter

for k = 1:(threshold - 1)
    priorxs(k) = priorx;
    priorvs(k) = priorv;
    gain = priorv*c/(c*priorv*c + sigmau2);
    yhat = c*priorx;
    postx = priorx + gain*(y(k) - yhat);
    postv = (1 - gain*c)*priorv;
    priorx = (a1 - b1*c)*postx + b1*r(k);
    priorv = (a1 - b1*c)^2*postv + b1^2*sigmau2 + sigmax2;
    postxs(k) = postx;
    postvs(k) = postv;
end
for k = threshold:60
    priorxs(k) = priorx;
    priorvs(k) = priorv;
    gain = priorv*c/(c*priorv*c + sigmau2);
    yhat = c*priorx;
    postx = priorx + gain*(y(k) - yhat);
    postv = (1 - gain*c)*priorv;
    priorx = (a2 - b2*c)*postx + b2*r(k);
    priorv = (a2 - b2*c)^2*postv + b2^2*sigmau2 + sigmax2;
    postxs(k) = postx;
    postvs(k) = postv;
end
% TODO:   Using the posteriors and priors above to compute the smoothed
%         expected state, variance, and covariance

xn_N = postxs(end);
pn_N = postvs(end);
xnN(end) = xn_N;
PnN(end) = pn_N;
for k = 59:-1:threshold
    J = (a2 - b2*c)*postvs(k)/priorvs(k+1);
    xn_N = postxs(k) + J*(xn_N - priorxs(k+1));
    pn1n_N = pn_N*J;
    pn_N = postvs(k) + J^2*(pn_N - priorvs(k+1));
    xnN(k) = xn_N;
    PnN(k) = pn_N;
    Pnp1nN(k) = pn1n_N;
end
for k = (threshold - 1):-1:1
    J = (a1 - b1*c)*postvs(k)/priorvs(k+1);
    xn_N = postxs(k) + J*(xn_N - priorxs(k+1));
    pn1n_N = pn_N*J;
    pn_N = postvs(k) + J^2*(pn_N - priorvs(k+1));
    xnN(k) = xn_N;
    PnN(k) = pn_N;
    Pnp1nN(k) = pn1n_N;
end