function [] = expectation_maximization(y,r)
load gem_data.mat
% This function links between the Kalman
% smoother (for the E-step) and the M-step functions
% This function plots the likelihoods, and obtains
% final parameter set used to make conclusions about the
% learner and simulate their behavior.
%%% Inputs: y - this is the observed subject reaching direction
%%%         r - this is the sequence of perturbations
%%% Outputs: none



% the initial parameter guess to use for GEM
parameters = [0.5 ; 0.5 ; 0.2 ; 0.2 ; 1 ;   20    ;   20    ;    0  ;    20];
%              a1   a2    b1    b2    c  sigma_x^2  sigma_u^2  x1_bar  P1_bar

% specifies the threshold trial number where predicted that a and b might
% change
threshold = 30;

% specifies the number of GEM iterations
num_iterations = 30;

% creates an array to store the likelihood after each iteration of GEM
likelihoods = zeros(num_iterations,1);


for n = 1 : num_iterations
    % performs the Kalman filter to generate the required estimates
    [xnN,PnN,Pnp1nN] = kalman_smoother(parameters,y,r,threshold);
    
    % compute a new parameter set use m_step
    [parameters,likelihood] = m_step(parameters,y,r,xnN,PnN,Pnp1nN,threshold);
    
    likelihoods(n) = likelihood;
end

% Looking at the final parameter values, we see that the washout is slow
% because even though a2 = 0.9384 < 1, the retention is still rather high
% so the subject is slow to forget previous trials. In addition, b2 =
% 0.0247, which indicates low error sensitivity, so the learner is not
% adjusting very fast to each error he makes. Therefore, the subject is
% slow to decay to learn when it changes to no perturbations.


figure
plot(likelihoods,'k')
ylabel('E[log-likelihood]')
xlabel('GEM iteration')
ax = gca;
ax.TickDir = 'out';
ax.XTick = [1,5:5:num_iterations];
xlim([1,num_iterations])

a1 = parameters(1);
a2 = parameters(2);
b1 = parameters(3);
b2 = parameters(4);
c = parameters(5);
sigmax2 = parameters(6);
sigmau2 = parameters(7);
x1 = parameters(8);
V1 = parameters(9);
yhats = zeros(60,1);
x = x1;
for k = 1:29
    yhats(k) = c*x;
    x = (a1 - b1*c)*x + b1*r(k);
end
for k = 30:60
    yhats(k) = c*x;
    x = (a2 - b2*c)*x + b2*r(k);
end

figure
hold on
plot(y,'k')
plot(r,'--k')
plot(yhats,'b')
legend('y','r','yhats')
ylabel('Reaching direction relative to target')
xlabel('Trial number')
hold off
ylim([-2,32])
ax = gca;
ax.TickDir = 'out';