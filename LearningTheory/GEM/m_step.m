function [parameters_new,likelihood_new] = m_step(parameters,y,r,xnN,PnN,Pnp1nN,threshold)
% This function uses fmincon to compute the expected complete
% log-likelihood function. 
%%% Inputs: y - the observed subject reaching direction
%%%         r - the sequence of perturbations
%%%         threshold - the trial number at which the a and b can
%%%                     change value
%%%         parameters - this is the current parameter set
%%%         xnN - N x 1 array containing the smoothed Kalman
%%%                expected states
%%%         PnN - N x 1 array containing the smoothed Kalman
%%%                state variances
%%%         Pnp1nN - N x 1 array containing the smoother Kalman
%%%                   covariance of x(n) and x(n+1) given all the data up
%%%                   to N. Its last entry is NaN, because we do not care
%%%                   abour Pnp1nN(N+1,N).
%%% Outputs: parameters_new - the parameter set that fmincon
%%%                           returns to us, which represents our new
%%%                           parameter set for the next iteration of GEMs
%%%          likelihoods_new - the expected complete log-likelihood
%%%                            for the final parameter set
%%%         


% stores the number of trials
N = length(y);

% stores the value of c
c = parameters(5);


%%% Implement the f_opt function using xnN, PnN, and Pnp1nN in the function
function [negated_likelihood] = f_opt(x)
    % the f_opt function is called by fmincon
    % which uses these values, along with xnN, PnN, and Pnp1nN to compute the
    % expected complete log-likelihood function
    a1 = x(1);
    a2 = x(2);
    b1 = x(3);
    b2 = x(4);
    sigmax2 = x(5);
    sigmau2 = x(6);
    x1 = x(7);
    V1 = x(8);
    
    % instantiated the log-likelihood to 0 to be updated
    likelihood = 0;
    
    firstsigma = zeros(1,N);
    secsigma = zeros(1,N-1);
    thirdsigma = zeros(1,N-1);
    for k = 1:N
        firstsigma(k) = y(k)^2 - 2*c*y(k)*xnN(k) + c^2*(PnN(k) + xnN(k)^2);
    end
    for k = 1:(threshold - 1)
        secsigma(k) = (1/(b1^2*sigmau2 + sigmax2))*(PnN(k+1) + xnN(k+1)^2 + (a1 - b1*c)^2*(PnN(k) + xnN(k)^2) + b1^2*r(k)^2 - 2*(a1 - b1*c)*(Pnp1nN(k) + xnN(k+1)*xnN(k)) - 2*b1*r(k)*xnN(k+1) + 2*(a1 - b1*c)*b1*r(k)*xnN(k));
    end
    for k = threshold:59
        secsigma(k) = (1/(b2^2*sigmau2 + sigmax2))*(PnN(k+1) + xnN(k+1)^2 + (a2 - b2*c)^2*(PnN(k) + xnN(k)^2) + b2^2*r(k)^2 - 2*(a2 - b2*c)*(Pnp1nN(k) + xnN(k+1)*xnN(k)) - 2*b2*r(k)*xnN(k+1) + 2*(a2 - b2*c)*b2*r(k)*xnN(k));
    end
    for k = 1:(threshold - 1)
        thirdsigma(k) = log(b1^2*sigmau2 + sigmax2);
    end
    for k = threshold:59
        thirdsigma(k) = log(b2^2*sigmau2 + sigmax2);
    end
    likelihood = (-1/(2*sigmau2))*sum(firstsigma) - (1/2)*sum(secsigma) - (1/(2*V1))*(PnN(1) + xnN(1)^2 - 2*xnN(1)*x1 + x1^2) - 60*log(sigmau2^0.5) - 0.5*sum(thirdsigma) - 0.5*log(V1);
    % This completes computing the likelihood.
    % We want to perform a maximization of the likelihood.
    % But fmincon performs minimiziation. So we negate the likelihood and
    % return this negated value.
    negated_likelihood = -likelihood;
end

% stores the parameters from the input parameter set
a1 = parameters(1);
a2 = parameters(2);
b1 = parameters(3);
b2 = parameters(4);
sigmax2 = parameters(6);
sigmau2 = parameters(7);
x1 = parameters(8);
V1 = parameters(9);

% constructs the vector to optimize
x_in = [a1;a2;b1;b2;sigmax2;sigmau2;x1;V1];

% specifies lower and upper bounds for our parameter search
%     a1     a2    b1      b1     sigmax2     sigmau2      x1      V1
lb = [0.1 ; 0.01 ; 0.01 ;  0.01  ;  0.01   ;   0.01   ;  -30  ; 0.001];
ub = [1   ;  1   ; 1    ;  1     ;  30     ;    30    ;   30  ; 30];

% makes sure that fmincon does not print results to screen
options = optimset('Display','off');

% performs constrained minimization
x_opt = fmincon(@f_opt,x_in,[],[],[],[],lb,ub,[],options);

% constructs the output parameter set
parameters_new = [x_opt(1);x_opt(2);x_opt(3);x_opt(4);c;x_opt(5);x_opt(6);x_opt(7);x_opt(8)];

% computes the resulting likelihood
likelihood_new = -f_opt(x_opt);

end