function [ Ks ] = calculateKalmanGains( pSteps, H, A, Qx, Qy, Se, Sx, Sxe, Gs, B, C1, C2 )
Ks = zeros(7,2,pSteps);
for m = 1:pSteps
    Se0 = Se;
    Sx0 = Sx;
    Sxe0 = Sxe;
    Ks(:,:,m) = Se0*H'*inv(H*Se0*H' + Qy);
    Se = Qx + A*(eye(7) - Ks(:,:,m)*H)*Se0*A' + B*C1*Gs(:,:,m)*Sx0*Gs(:,:,m)'*C1'*B' + B*C2*Gs(:,:,m)*Sx0*Gs(:,:,m)'*C2'*B';
    Sx = (A - B*Gs(:,:,m))*Sx0*(A - B*Gs(:,:,m))' + (A - B*Gs(:,:,m))*Sxe0*(A*Ks(:,:,m)*H)' + A*Ks(:,:,m)*H*Sxe0'*(A - B*Gs(:,:,m))' + A*Ks(:,:,m)*H*Se0*A';
    Sxe = (A - B*Gs(:,:,m))*Sxe0*(eye(7) - Ks(:,:,m)*H)'*A';
end