function [ Gs ] = calculateFeedbackGains( L, C1, C2, A, B, Wx, We, w, pSteps, Ks, T, H)
Gs = zeros(2,7,pSteps);
for m = pSteps-1:-1:1
    Cx = C1'*B'*Wx*B*C1 + C2'*B'*Wx*B*C2;
    Ce = C1'*B'*We*B*C1 + C2'*B'*We*B*C2;
    Gs(:,:,m) = inv(L + Cx + Ce + B'*Wx*B)*B'*Wx*A;
    We = (A - A*Ks(:,:,m)*H)'*We*(A - A*Ks(:,:,m)*H) + Gs(:,:,m)'*B'*Wx*A;
    Wx = H'*T(:,:,m)*H + A'*Wx*A - Gs(:,:,m)'*B'*Wx*A;
end