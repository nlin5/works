function [ eyeposone, headposone, gazeone, eyeposmean, headposmean, gazemean, Ks, Gs ] = simulateCase( x0, A,B, pSteps, k1Steps, T, Thold, numAverage, ...
    Qx, Qy, H, C1, C2, L, Delta, goal, num1, num2 )
Wx = H'*T(:,:,pSteps)*H; % seeding the parameters
We = zeros(7,7);
w = 0;
Se = zeros(7,7);
Sx = x0*x0';
Sxe = zeros(7,7);
Gs = zeros(2,7,pSteps);
for k = 1:10 % calculating Ks and Gs and iterating 10 times so they converge
[ Ks ] = calculateKalmanGains( pSteps, H, A, Qx, Qy, Se, Sx, Sxe, Gs, B, C1, C2 );
[ Gs ] = calculateFeedbackGains( L, C1, C2, A, B, Wx, We, w, pSteps, Ks, T, H );
end
eyepos = zeros(numAverage,pSteps); % store eye positions over time for each simulation
headpos = zeros(numAverage,pSteps); % store head positions over time for each simulation
for k = 1:numAverage % simulate numAverage times and record x's
    [ x0s ] = simulateSystem( A, B, C1, C2, H, Qx, Qy, Gs, x0, pSteps, Ks, Thold );
    eyepos(k,:) = x0s(1,1,:);
    headpos(k,:) = x0s(4,1,:);
end
select = randi([1 numAverage]); % randomly select a simulation to plot
eyeposone = eyepos(select,:);
headposone = headpos(select,:);
gazeone = eyeposone + headposone;
eyeposmean = mean(eyepos); % find the mean trajectories
headposmean = mean(headpos);
gazemean = eyeposmean + headposmean;