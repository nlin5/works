function [ x0s ] = simulateSystem( A, B, C1, C2, H, Qx, Qy, Gs, x0, pSteps, Ks, Thold )
x0s = zeros(7,1,pSteps); % record the states over time steps
x = x0; % set initial state
for k = 1:pSteps
    u = -Gs(:,:,k)*x; % generate optimal motor command
    yhat = H*x; % make prediction
    x0s(:,:,k) = x + Ks(:,:,k)*((H*x + mvnrnd(zeros(2,1),Qy)') - yhat); % store posterior state estimate
    if k <= Thold % if there is holding period, set head position and velocity to 0
        x0s(4:5,:,k) = [0; 0];
    end
    x = A*x0s(:,:,k) + B*u + mvnrnd(zeros(7,1),Qx)' + B*(C1*u*normrnd(0,1) + C2*u*normrnd(0,1)); % update state equation
end