% Author: Nole Lin
clc
clear
load phoxyz
nontarget = {'a', 'the', 'of', 'A', 'The', 'Of'}; % will remove non-target words
numprompt = 'How many sentences will you be entering? ';
num = input(numprompt);
reals = cell(1,num);
guesss = cell(1,num);
for z = 1:num
    prompt1 = 'Enter the sentence you thought you heard without punctuation: ';
    guess = input(prompt1, 's');
    prompt2 = 'Enter the correct sentence without punctuation: ';
    real = input(prompt2, 's');
    real = strsplit(real, ' ');
    guess = strsplit(guess, ' ');
    for k = length(real):-1:1
        for m = 1:6
            if strcmp(real{k}, nontarget{m}) == 1
                real(k) = [];
            end
        end
    end
    for k = length(guess):-1:1
        for m = 1:6
            if strcmp(guess{k}, nontarget{m}) == 1
                guess(k) = [];
            end
        end
    end
    reals{z} = real;
    guesss{z} = guess;
end

guessphonemes = cell(num,1);
realphonemes = cell(num,1);
for m = 1:num
    guess2 = {};
    real2 = {};
    for k = 1:length(guesss{m})
        guessurl = strrep(guesss{m}{k}, '', '%27');
        guessurl = strcat('http://www.speech.cs.cmu.edu/cgi-bin/cmudict?in=',guessurl);
        guesshtml = urlread(guessurl);
        guesstt = strfind(guesshtml,'<tt>');
        guessendtt = strfind(guesshtml,'</tt>');
        guess2{end+1} = guesshtml(guesstt(2)+4:guessendtt(2)-3);
    end
    for k = 1:length(reals{m})
        realurl = strrep(reals{m}{k}, '', '%27');
        realurl = strcat('http://www.speech.cs.cmu.edu/cgi-bin/cmudict?in=',realurl);
        realhtml = urlread(realurl);
        realtt = strfind(realhtml,'<tt>');
        realendtt = strfind(realhtml,'</tt>');
        real2{end+1} = realhtml(realtt(2)+4:realendtt(2)-3);
    end
    guessphonemes{m} = strjoin(guess2);
    realphonemes{m} = strjoin(real2);
end

for k = 1:num
    guess2 = strsplit(guessphonemes{k});
    real2 = strsplit(realphonemes{k});
    if length(real2) > length(guess2)
        diff = length(real2) - length(guess2);
        guessphonemes{k} = strjoin(horzcat(guess2, repmat({'qq'},1,diff)));
    end
end
totp = cell(num,1);
uniqr = [];
for z = 1:num
    sent = strsplit(realphonemes{z});
    gsent = strsplit(guessphonemes{z});
    realtot = zeros(1,length(sent));
    uniqr = [uniqr ' ' realphonemes{z}];
    for k = 1:length(sent)
        if sent{k} == gsent{k}
            realtot(k) = 1;
        else
            realtot(k) = 0;
        end
        
    end
    totp{z} = realtot;
end
uniqr(1) = [];
uniqr2 = unique(strsplit(uniqr));
uniqr = strsplit(uniqr);
uniqcount = zeros(2,length(uniqr2));
correct = horzcat(totp{:});
for k = 1:length(uniqr)
    for m = 1:length(uniqr2)
        if uniqr{k} == uniqr2{m}
            uniqcount(1,m) = uniqcount(1,m) + correct(k);
            uniqcount(2,m) = uniqcount(2,m) + 1;
        end
    end
end
percent = (uniqcount(1,:)./uniqcount(2,:))*100;
disp('Unique Phonemes and their Percent Accuracy')
disp(vertcat(uniqr2,num2cell(percent)))
for k = 1:length(uniqr2)
    if length(strfind('AEIOU',uniqr2{k}(1))) ~= 0 % if the phoneme is a vowel
        try
            coord = phoxyz(uniqr2{k});
        catch
            disp('You might have misspelled something')
            return
        end
        figure(1)
        hold on
        scatter(coord(1), coord(2), 350, percent(k), 'filled')
        text(coord(1), coord(2), uniqr2{k}, 'Color', [219/256,147/256,112/256], 'HorizontalAlignment','center', 'VerticalAlignment','middle')
        set(gca,'clim',[0,100]);
        colorbar;
    else % phoneme is a consonant
        try
            coord = phoxyz(uniqr2{k});
        catch
            disp('You might have misspelled something')
            return
        end
        if coord(3) == 1
            figure(2)
            hold on
            scatter(coord(1), coord(2), 350, percent(k), 'filled')
            text(coord(1), coord(2), uniqr2{k}, 'Color', [219/256,147/256,112/256], 'HorizontalAlignment','center', 'VerticalAlignment','middle')
            set(gca,'clim',[0,100]);
            colorbar;
        else
            figure(3)
            hold on
            scatter(coord(1), coord(2), 350, percent(k), 'filled')
            text(coord(1), coord(2), uniqr2{k}, 'Color', [219/256,147/256,112/256], 'HorizontalAlignment','center', 'VerticalAlignment','middle')
            set(gca,'clim',[0,100]);
            colorbar;
        end
    end
end
figure(1)
title('Vowels')
xlabel('Place')
set(gca,'XLim',[0 8])
ax = gca;
ax.XTickLabelRotation = 45;
set(gca,'XTickLabel',{' ', 'Front', ' ', ' ', 'Center', ' ', ' ', 'Back', ' '});
ylabel('Opening')
set(gca,'YLim',[0 6])
set(gca,'YTickLabel',{' ', 'High', ' ', 'Mid', ' ', 'Low', ' '})
colorbar('Ticks',linspace(0,100,11),...
         'TickLabels',{'0%', '10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%'})
figure(2)
title('Consonants (Voiced)')
xlabel('Place')
set(gca,'Xlim',[0 7])
ax = gca;
ax.XTickLabelRotation = 45;
set(gca,'Xticklabel',{' ', 'Bilabial', 'Labiodental', 'Lingadental', 'Alveolar', 'Palatal', 'Velar', '  '});
ylabel('Manner')
set(gca,'ylim',[0 6])
ax = gca;
%ax.YTickLabelRotation = 45;
set(gca,'Yticklabel',{' ', 'Sonorant', 'Fricative', 'Affricate', 'Stop', 'Nasal', '  '});
colorbar('Ticks',linspace(0,100,11),...
         'TickLabels',{'0%', '10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%'})
figure(3)
title('Consonants (Unvoiced)')
xlabel('Place')
set(gca,'Xlim',[0 7])
ax = gca;
ax.XTickLabelRotation = 45;
set(gca,'Xticklabel',{' ', 'Bilabial', 'Labiodental', 'Lingadental', 'Alveolar', 'Palatal', 'Velar', '  '});
ylabel('Manner')
set(gca,'ylim',[0 6])
ax = gca;
%ax.YTickLabelRotation = 45;
set(gca,'Yticklabel',{' ', 'Sonorant', 'Fricative', 'Affricate', 'Stop', 'Nasal', '  '});
colorbar('Ticks',linspace(0,100,11),...
         'TickLabels',{'0%', '10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%'})